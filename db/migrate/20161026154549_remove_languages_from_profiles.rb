class RemoveLanguagesFromProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :languages, :string
  end
end
