class RemoveHeightFromProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :height, :string
  end
end
