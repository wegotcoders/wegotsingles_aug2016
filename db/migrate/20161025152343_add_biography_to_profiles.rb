class AddBiographyToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :biography, :text
  end
end
