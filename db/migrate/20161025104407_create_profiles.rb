class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true, foreign_key: true
      t.string :location
      t.string :gender
      t.integer :weight
      t.string :height
      t.string :ethnicity
      t.string :languages
      t.string :education
      t.string :star_sign

      t.timestamps null: false
    end
  end
end
