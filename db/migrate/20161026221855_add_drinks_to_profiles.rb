class AddDrinksToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :drinks, :string
  end
end
