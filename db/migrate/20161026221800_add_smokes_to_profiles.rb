class AddSmokesToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :smokes, :string
  end
end
