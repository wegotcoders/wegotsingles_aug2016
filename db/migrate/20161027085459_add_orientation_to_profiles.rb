class AddOrientationToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :orientation, :string, array: true, default: []
  end
end
