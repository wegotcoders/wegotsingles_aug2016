class AddLookingForToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :looking_for, :string, array: true, default: []
  end
end
