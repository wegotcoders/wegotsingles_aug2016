# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Conversation.destroy_all
Profile.destroy_all
Conversation.destroy_all
User.destroy_all

@barvis = User.create(email: "SexyBarvis69", email: "barvis.fuchschittehr@gmail.com", password: "123456", password_confirmation: "123456", date_of_birth: Date.today - 37.years)
@barvis.confirm
@barvis.create_profile(
  weight: 73,
  height: 201,
  ethnicity: "Non-White-Irish",
  education: "phD",
  biography: "Buenas Dias Amigos, soy su creado obiente",
  religion: "Heathen",
  languages: "Spanish"
)

@janet = User.create(email: "janet101", email: "janet123@hotmail.com", password: "123456", password_confirmation: "123456", date_of_birth: Date.today - 37.years)
@janet.confirm
@janet.create_profile(
  gender: "Demigirl",
  weight: 51,
  height: 153,
  ethnicity: "Aboriginal",
  education: "phD",
  biography: "Janet",
  religion: "Buddhist",
  languages: "Polish"
)

@bjork = User.create(email: "Bjork", email: "bjork@gmail.com", password: "123456", password_confirmation: "123456", date_of_birth: Date.today - 37.years)
@janet.confirm
@bjork.create_profile(
  gender: "Female",
  weight: 40,
  height: 110,
  ethnicity: "White",
  education: "phD",
  biography: "...",
  religion: "Heathen",
  languages: "English"
)

@frank = User.create(email: "ZombieWoof", email: "zappa@gmail.com", password: "123456", password_confirmation: "123456", date_of_birth: Date.today - 37.years)
@frank.confirm
@frank.create_profile(
  gender: "Male",
  weight: 70,
  height: 190,
  ethnicity: "Non-White-Irish",
  education: "High School",
  biography: "Lick my aura Dora.",
  religion: "Atheist",
  languages: "English"
)

@brunhilde = User.create(email: "Valkyriexxx", email: "brunny@gmail.com", password: "123456", password_confirmation: "123456", date_of_birth: Date.today - 37.years)
@brunhilde.confirm
@brunhilde.create_profile(
  gender: "Female",
  weight: 110,
  height: 140,
  ethnicity: "White",
  education: "Uneducated Brute",
  biography: "Real girls have curves",
  religion: "Agnostic",
  languages: "English"
)

User.all.each do |user|
  user.confirm
end

