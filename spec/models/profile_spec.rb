require 'rails_helper'

RSpec.describe Profile, type: :model do

  before do
    @bob = User.create!(email: "bob@bobland.net", password: "123456", password_confirmation: "123456")
    if @bob.valid?
      @profile = @bob.create_profile
    else
      @bob
    end
  end


  describe "User profile" do
    it "should not create a profile if user already exist" do
      @bob2 = User.create(email: "bob@bobland.net", password: "123456", password_confirmation: "123456")
      if @bob2.valid?
        @profile = @bob2.create_profile!
      else
        @bob2
      end
      expect(Profile.last.user_id).to eq(@bob.profile.user_id)
    end
  end
end
