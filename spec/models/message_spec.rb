require 'rails_helper'

RSpec.describe Message, type: :model do
  before do
    @bob = User.create(email: "bob@bob.net", password: "123456", password_confirmation: "123456")
    @alice = User.create(email: "alice@alice.net", password: "123456", password_confirmation: "123456")
    @conversation1 = Conversation.create(sender: @alice, recipient: @bob)
    @message = @conversation1.messages.create(user: @bob, body: "Hi Lassie!")
  end

  describe "Message" do
    context "With valid creation criteria" do
      it "should be valid with all criteria" do
        expect(@message).to be_valid
      end
    end

    context "With invalid creation criteria" do
      before do
        @msg_no_user = @conversation1.messages.create(body: "Hey there Lassie")
        @msg_no_body = @conversation1.messages.create(user: @alice)
        @msg_no_convo = Message.create(user: @alice, body: "Hey there laddie")
      end
      it "should not be valid without a user" do
        expect(@msg_no_user).to_not be_valid
      end
      it "should not be valid without a body" do
        expect(@msg_no_body).to_not be_valid
      end
      # it "should not be valid without a conversation" do
      #   expect(@msg_no_convo).to_not be_valid
      #   expect(@msg_no_convo).to have_id
      # end
    end
  end

end
