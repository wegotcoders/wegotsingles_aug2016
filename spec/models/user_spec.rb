require 'rails_helper'

RSpec.describe User, type: :model do

  before do
    @bob = User.create!(email: "bob@bobland.net", password: "123456", password_confirmation: "123456")
    @alice = User.create!(email: "alice@alice.net", password: "123456", password_confirmation: "123456")
  end

  describe "User conversations" do
    it "should be able to see conversations as a sender" do
      @conversation2 = @bob.conversations.create!(recipient: @alice)
      expect(@bob.my_conversations).to include @conversation2
      expect(@bob.my_conversations.count).to eq 1
    end
    it "should be able to see conversations as a recipient" do
      @conversation1 = @alice.conversations.create!(recipient: @bob)
      expect(@bob.my_conversations).to include @conversation1
      expect(@bob.my_conversations.count).to eq 1
    end
  end

end
