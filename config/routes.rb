Rails.application.routes.draw do

  default_url_options :host => "localhost"
  devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: 'users/registrations'
  }

  root 'pages#home'

  # resources :users do
    resources :profiles, only: [:index, :show, :edit, :update]
  # end

  resources :conversations, only: [:index, :create, :show, :update, :destroy] do
    resources :messages, only: [:create, :update, :destroy]
  end

end

