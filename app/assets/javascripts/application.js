// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require tether
//= require bootstrap
//= require filterrific/filterrific-jquery
//= require typed
//= require jquery_ujs
//= require jquery.remotipart
//= require_tree .

$(function(){
    $(".element").typed({
        strings: ["We got singles", "We got couples", "We got monsters"],
        typeSpeed: 50,
        shuffle: false,
        highlight: true,
        loop: false,
        startDelay: 1,
        backSpeed: 50,
        backDelay: 1000,
        showCursor: false,
        cursorChar: "|"
//      cursor: 'typed-cursor'

    })
});
