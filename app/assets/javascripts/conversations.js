// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var csrf = $("meta[name=csrf-token]").attr("content");

$('.new_message').on('keydown', function (e) {
  var subBtn = $(e.currentTarget).find('.submitBtn');
  if (e.keyCode == 13) {
    subBtn.click();
  }
});

$('.edit_message').on("ajax:success", function (e, data) {
  var windows = $('.inbox').find('.outerContainer');
  for (var i = 0 ; i < windows.length ; ++i) {
    $(windows[i]).hide();
  }
  $('#container_' + data.id).toggle();
});
