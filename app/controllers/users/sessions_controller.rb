class Users::SessionsController < Devise::SessionsController
# before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    redirect_to new_user_registration_path
    # super
  end

  # POST /resource/sign_in
  def create
    super do |resource|
	    if resource.valid?
	      flash[:notice] = "You have successfully logged in"
	    else
				flash[:error] = "Sorry your credentials are incorrect"
	    end
    end
  end


  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
