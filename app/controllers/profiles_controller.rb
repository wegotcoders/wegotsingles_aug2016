class ProfilesController < ApplicationController

  before_action :find_profile

  def index
    # @profiles = Profile.all
    @filterrific = initialize_filterrific(Profile, params[:filterrific], select_options: {
        with_gender: Profile.options_for_gender,
        with_ethnicity: Profile.options_for_ethnicity,
        with_smoke: Profile.options_for_smokes_or_drinks,
        with_drink: Profile.options_for_smokes_or_drinks,
        with_orientation: Profile.options_for_orientation
      }
    ) or return
    @profiles = @filterrific.find.page(params[:page])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @conversation = Conversation.new
  end

  def update
    # TODO: @profile.pictures << profile_params[:pictures]
    @profile.update(profile_params)
    if remotipart_submitted?
      render json: @profile
    else
      redirect_to profile_path(@profile)
    end
  end

  private

  def find_profile
    @profile = Profile.find_by(id: params[:id])
  end

  def profile_params
    params.require(:profile).permit(:biography, :gender, :ethnicity, :weight, :height, :smokes, :drinks, :education, :profession, :religion, orientation: [], looking_for: [], languages: [], pictures: [])
  end

end
