class ConversationsController < ApplicationController

  before_action :find_conversation, only: [:show, :update]

  def index
    @my_conversations = current_user.my_conversations.order(:updated_at).reverse_order
    @message = Message.new
  end

  def new
    @conversation = Conversation.new
  end

  def create
    conversation = Conversation.where("(sender_id =? AND recipient_id =?) OR (sender_id =? AND recipient_id =?)", conversation_params[:sender_id], conversation_params[:recipient_id], conversation_params[:recipient_id], conversation_params[:sender_id]).first
    unless conversation.nil?
      message_params = conversation_params[:messages_attributes]["0"].to_h.merge(conversation_id: conversation.id)
      conversation.messages.create(message_params)
    else
      conversation = current_user.conversations.create(conversation_params)
    end
    redirect_to conversation_path(conversation)
  end

  def show
    @message = Message.new
  end

  def update
  end

  private

  def find_conversation
    @conversation = Conversation.find(params[:id])
  end

  # Need to adapt this for sender / receiver --> Cannot call @conversation.user

  # def authorisation
  #   @conversation = Conversation.find(params[:id])
  #   if @conversation.user == current_user
  #     @conversation
  #   else
  #     flash[:notice] = "ERROREZ. Dis ain't belongin tu yousa"
  #     nil
  #   end
  # end

  def conversation_params
    params.require(:conversation).permit(:id, :sender_id, :recipient_id, messages_attributes: [:id, :body, :user_id])
  end

end
