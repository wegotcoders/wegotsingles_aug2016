class MessagesController < ApplicationController

  before_action :find_message, only: [:update, :destroy]

  def create
    conversation = current_user.my_conversations.find(params[:conversation_id])
    my_message_params = message_params.to_h
    my_message_params = my_message_params.merge(user: current_user)
    @message = conversation.messages.create(my_message_params)
    respond_to do |format|
      format.js
    end
  end

  def update
    @message.update(message_params)
    render json: @message.conversation
  end

  def destroy

  end

  private

  def find_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:id, :body, :user_id, :conversation_id, :is_read)
  end

end
