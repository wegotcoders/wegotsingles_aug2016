class Profile < ActiveRecord::Base
  belongs_to :user
  mount_uploaders :pictures, PicturesUploader

  def ethnicities
    ["White", "Non-White Irish", "Non-White-Irish", "Black", "Asian", "Indian",
     "Middle Eastern", "Native American", "Pacific Islander", "Hispanic/Latin", "Other"
    ]
  end

  def religions
    ["Christian", "Muslim", "Buddhist", "Taoist", "Hindu", "Atheist", "Agnostic", "Heathen"]
  end

  def list_languages
    ["English", "French", "Spanish", "Portugese", "Minion", "Polish", "Elvish",
     "Chinese (Mandarin)", "Chinese (Cantonese)", "Japanese"
    ]
  end

  def educations
    ["Uneducated Brute", "High School", "University", "Postgraduate", "PhD"]
  end

  def list_smokes_or_drinks
    ["Never", "Sometimes", "Frequently"]
  end

  def list_genders
    ["Male", "Female", "Other", "Agender", "Androgyne", "Bi-gender", "Butch", "Demiboy", "Demigirl", "Genderfluid", "Two-Spirit", "Yinyang ren"]
  end

  def list_looking_for
    ["Short-term Dating", "Long-term Dating", "Casual Sex", "New Friends"]
  end

  def orientations
    ["Heterosexual","Bisexual","Homosexual","Asexual","Androgynosexual","Queer",
     "Omnisexual","Heteroflexible","Gynosexual","Demisexual","Androsexual","Other"]
  end

  def determine_star_sign
    if (self.user.date_of_birth.month == 1 && self.user.date_of_birth.day >= 20) || 
    (self.user.date_of_birth.month == 2 && self.user.date_of_birth.day <= 18)
      "Aquarius"
    elsif (self.user.date_of_birth.month == 2 && self.user.date_of_birth.day >= 19) || 
    (self.user.date_of_birth.month == 3 && self.user.date_of_birth.day <= 20)
      "Pisces"
    elsif (self.user.date_of_birth.month == 3 && self.user.date_of_birth.day >= 21) || 
    (self.user.date_of_birth.month == 4 && self.user.date_of_birth.day <= 19)
      "Aries"
    elsif (self.user.date_of_birth.month == 4 && self.user.date_of_birth.day >= 20) || 
    (self.user.date_of_birth.month == 5 && self.user.date_of_birth.day <= 20)
      "Taurus"
    elsif (self.user.date_of_birth.month == 5 && self.user.date_of_birth.day >= 21) || 
    (self.user.date_of_birth.month == 6 && self.user.date_of_birth.day <= 20)
      "Gemini"
    elsif (self.user.date_of_birth.month == 6 && self.user.date_of_birth.day >= 21) || 
    (self.user.date_of_birth.month == 7 && self.user.date_of_birth.day <= 22)
      "Cancer"
    elsif (self.user.date_of_birth.month == 7 && self.user.date_of_birth.day >= 23) || 
    (self.user.date_of_birth.month == 8 && self.user.date_of_birth.day <= 22)
      "Leo"
    elsif (self.user.date_of_birth.month == 8 && self.user.date_of_birth.day >= 23) || 
    (self.user.date_of_birth.month == 9 && self.user.date_of_birth.day <= 22)
      "Virgo"
    elsif (self.user.date_of_birth.month == 9 && self.user.date_of_birth.day >= 23) || 
    (self.user.date_of_birth.month == 10 && self.user.date_of_birth.day <= 22)
      "Libra"
    elsif (self.user.date_of_birth.month == 10 && self.user.date_of_birth.day >= 23) || 
    (self.user.date_of_birth.month == 11 && self.user.date_of_birth.day <= 21)
      "Scorpio"
    elsif (self.user.date_of_birth.month == 11 && self.user.date_of_birth.day >= 22) || 
    (self.user.date_of_birth.month == 12 && self.user.date_of_birth.day <= 21)
      "Sagittarius"
    elsif (self.user.date_of_birth.month == 12 && self.user.date_of_birth.day >= 22) || 
    (self.user.date_of_birth.month == 1 && self.user.date_of_birth.day <= 19)
      "Capricorn"  
    end
  end

  filterrific(
       # default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters: [
          :with_gender,
          :with_ethnicity,
          :with_smoke,
          :with_drink,
          :with_orientation
      ]
  )
  scope :with_gender, lambda { |genders|
    where(gender: [*genders])
  }

  scope :with_ethnicity, lambda { |ethnicities|
    where(ethnicity: [*ethnicities])
  }

  scope :with_drink, lambda { |drinks|
    where(drinks: [*drinks])
  }

  scope :with_smoke, lambda { |smokes|
    where(smokes: [*smokes])
  }

  scope :with_orientation, lambda {|orientations|
    where(orientation: [*orientations])
  }

  def self.options_for_orientation
    [
            ["Heterosexual", "Heterosexual"],
            ["Bisexual", "Bisexual"],
            ["Homosexual","Homosexual"],
            ["Asexual","Asexual"],
            ["Androgynosexual","Androgynosexual"],
            ["Queer","Queer"],
            ["Omnisexual", "Omnisexual"],
            ["Heteroflexible","Heteroflexible"],
            ["Gynosexual", "Gynosexual"],
            ["Demisexual","Demisexual"],
            ["Androsexual","Androsexual"],
            ["Other", "Other"]
    ]
  end

  def self.options_for_smokes_or_drinks
    [
        ["Never", "Never"],
        ["Sometimes", "Sometimes"],
        ["Frequently", "Frequently"]

    ]
  end

  def self.options_for_ethnicity
    [
        ["White", "White"],
        ["Non-White Irish","Non-White Irish"],
        ["Non-White-Irish","Non-White-Irish"],
        ["Black","Black"],
        ["Aboriginal","Aboriginal"],
        ["Asian","Asian"],
        ["Indian", "Indian"],
        ["Middle Eastern","Middle Eastern"],
        ["Native American", "Native American"],
        ["Pacific Islander","Pacific Islander"],
        ["Hispanic/Latin","Hispanic/Latin"],
        ["Other", "Other"]
    ]
  end


  def self.options_for_gender
    [
        ['Male', 'Male'],
        ['Female', 'Female'],
        ['Other', 'Other'],
        ['Agender', 'Agender' ],
        ['Androgyne', 'Androgyne' ],
        ['Bi-gender', 'Bi-gender'],
        ['Butch', 'Butch'],
        ['Demiboy', 'Demiboy'],
        ['Demigirl', 'Demigirl'],
        ['Genderfluid', 'Genderfluid'],
        ['Two-Spirit', 'Two-Spirit'],
        ['Yinyang ren', 'Yinyang ren']
    ]
  end
end
