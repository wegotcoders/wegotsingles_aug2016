class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :validatable, :lockable

  has_many :conversations, foreign_key: :sender_id
  has_one :profile

  def my_conversations
    Conversation.where('conversations.sender_id =? OR conversations.recipient_id =?', self.id, self.id)
  end

end
