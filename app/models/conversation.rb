class Conversation < ActiveRecord::Base
  belongs_to :sender, foreign_key: :sender_id, class_name: 'User'
  belongs_to :recipient, foreign_key: :recipient_id, class_name: 'User'
  has_many :messages, dependent: :destroy
  accepts_nested_attributes_for :messages

  validate :duplicate_conversation
  validates_presence_of :sender, :recipient
  
  private

  def duplicate_conversation
    # TODO: Refactor this code into a SQL statement
    duplicate_a = Conversation.where(sender: self.sender, recipient: self.recipient)
    duplicate_b = Conversation.where(sender: self.recipient, recipient: self.sender)
    unless duplicate_a.empty? && duplicate_b.empty?
      self.errors.add(:duplicate_conversation, "A conversation already exists between this sender and recipient")
    end
  end

end
