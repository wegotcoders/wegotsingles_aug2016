Feature: Conversation

  Scenario: View all conversations
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the "conversations" page
    Then the page contains "Sure, Alice, lav a dag"

  Scenario: View a conversation
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the specific "conversation" page
    Then the page contains "Hey Bobby, do you like dags?"
    And the page contains "Sure, Alice, lav a dag"
    # TODO : Set in show method for conversation controller all messages to be read
    # And all relevant messages are set to read

  @poltergeist
  @wip
  Scenario: Start a conversation
    Given sample users have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the specific "profile" page
    And they click "Message Me"
    And they fill in the "conversation[messages_attributes][0][body]" field with "Alright Chicken!"
    And they press "ENTER" on "textarea"
    Then a new Conversaton is created
    And a new Message is created
    And they are redirected to the specific "conversation" page
    And the page contains "Alright Chicken!"

  @poltergeist
  Scenario: Avoid duplicate conversation
    Given sample users have been created
    And sample conversations have been created
    And they have logged in as "bob@bobland.net"
    And a conversation already exists between "bob@bobland.net" and "alice@alice.net"
    When they navigate to the specific "profile" page
    And they click "Message Me"
    And they fill in the "conversation[messages_attributes][0][body]" field with "Alright Chicken!"
    And they press "ENTER" on "textarea"
    Then they are redirected to the specific "conversation" page
    And a new Conversation is not created
    And a new Message is created
    And the page contains "Alright Chicken!"

  Scenario: Delete a conversation
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the "conversations" page
    And they click "End Conversation"
    Then the messages are deleted
    And the conversation is deleted
    And they can no longer see "Hey Bobby, do you like dags?"
    And they can no longer see "alice@alice.net"
