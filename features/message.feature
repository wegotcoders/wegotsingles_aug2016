Feature: Message

  Scenario: Read a message
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the specific "conversation" page
    Then the page contains "Hey Bobby, do you like dags?"
    And the page contains "Sure, Alice, lav a dag"

    @poltergeist
  Scenario: Reply to a message
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the specific "conversation" page
    And they fill in the "message_body" field with "Whats up Willis?"
    And they press "ENTER" on "textarea"
    Then a new "Message" is created
    And the page contains "Whats up Willis"

  Scenario: Edit a message
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the specific "conversation" page
    And the page contains "Sure, Alice, lav a dag"
    And they click "Edit Message"
    And they fill in the "coversation[messages_attributes][0][body]" field with "Damn Grrl, I like the look of your jupe"
    And they press "ENTER"
    Then the message is edited
    And the page contains "Damn Grrl, I like the look of your jupe"

  Scenario: Delete a message
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    When they navigate to the specific "conversation" page
    And the page contains "Sure, Alice, lav a dag"
    And they click "Delete Message"
    Then the message is deleted
    And they can no longer see "Sure, Alice, lav a dag"

    @poltergeist
  Scenario: Update message once read
    Given sample users have been created
    And sample conversations have been created
    And sample messages have been created
    And they have logged in as "bob@bobland.net"
    And their latest message is unread
    When they navigate to the "conversations" page
    Then the page contains the element "div.unread"
    When they click on the "mark_as_read" link
    Then the message is updated as read
    And they are redirected to the specific "conversation" page
    When they navigate to the "conversations" page
    Then the page contains the element "div.read"

