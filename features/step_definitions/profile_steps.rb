Given(/^they are logged in as "(.*?)"$/) do |email|
  @user = User.find_by(email: email)
  page.set_rack_session('warden.user.user.key' => User.serialize_into_session(@user))
  @profile = @user.profile
end

Given(/^They have gone to the route "(.*?)" profile$/) do |page|
 # visit get_named_route(page)
 visit profile_path(@jorvis)
end

# When(/^They click the "(.*?)" button \(for profile\)$/) do |button|
#   click_on button
# end

When(/^They fill in the "(.*?)" field with "(.*?)" \(for profile\)$/) do |field, content|
  save_and_open_page
  fill_in field, with: content
end

When(/^they fill in the upload picture form with a valid image$/) do
    attach_file("profile_pictures", File.join(Rails.root, "/public/uploads/profile/pictures/artsfon.com-77419.jpg"))
end

Then(/^the page contains the id of "(.*?)"$/) do |content|
  expect(find_by_id(content))
end

Then(/^the page contains the id of \\"(.*?)" with content "(.*?)"$/) do |id, content|
  expect(find_by_id(id[0..-2])[:alt]).to have_content(content)
end


When(/^they click the "(.*?)" button$/) do |button|
  click_on button
end

When(/^they choose "(.*?)" from the "(.*?)" dropdown list$/) do |option, dropdown|
  select option, :from => dropdown
end

# Then(/^Jorvis' "(.*?)" is set to "(.*?)"$/) do |property, value|
#   expect(@jorvis.profile.property).to eq(value)
# end

Then(/^Jorvis' education is set to High School$/) do
  # expect(@jorvis.profile.education).to eq("High School")
  page.should have_content("High School")
end

Then(/^Jorvis' drinks is set to Sometimes$/) do
  # expect(@jorvis.profile.drinks).to eq("Sometimes")
  page.should have_content("Sometimes")
end

Then(/^Jorvis' smokes is set to Frequently$/) do
  # expect(@jorvis.profile.smokes).to eq("Frequently")
  page.should have_content("Frequently")
end

Then(/^Jorvis' interested in is set to Male and Female$/) do
  # expect(@jorvis.profile.orientation).to eq(["Male", "Female"])
  page.should have_content("Male")
  page.should have_content("Female")
end

Then(/^Jorvis' height is set to 180cm$/) do
  # expect(@jorvis.profile.height).to eq(180)
  page.should have_content("180cm")
end

Then(/^Jorvis' weight is set to 95kg$/) do
  # expect(@jorvis.profile.weight).to eq(180)
  page.should have_content("95kg")
end

Then(/^Jorvis' languages is set to English, French, Spanish, Japanese, Elvish, Minion$/) do
  # expect(@jorvis.profile.orientation).to eq(["English", "French", "Spanish", "Japanese", "Elvish", "Minion"])
  page.should have_content("English")
  page.should have_content("French")
  page.should have_content("Spanish")
  page.should have_content("Japanese")
  page.should have_content("Elvish")
  page.should have_content("Minion")
end

Then(/^Jorvis' religion is set to Hindu$/) do
  # expect(@jorvis.profile.religion).to eq("Hindu")
  page.should have_content("Hindu")
end

Then(/^Jorvis' biography is updated$/) do
  page.should have_content("I'm a real good guy and I definitely don't drown puppies")
end

When(/^they click the "(.*?)" checkbox for "(.*?)"$/) do |value, property|
  find(:css, "##{property}#{value}").set(true)
end

