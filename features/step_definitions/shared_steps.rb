Given(/^sample users have been created$/) do
  @jorvis = User.new(email: "jorvis@barvis.com", password: "123456",
                     email: "SexyJorvis", date_of_birth: "1980/01/01")
  @jorvis.skip_confirmation!
  @jorvis.confirm
  @jorvis.save!
  Profile.create(user: @jorvis, biography: "I'm ok.")
  @snarvis = User.new(email: "snarvis@barvis.com", password: "123456",
                      email: "SexySnarvis", date_of_birth: "1990/01/01")
  @snarvis.skip_confirmation!
  @snarvis.confirm
  @snarvis.save!
  Profile.create(user: @snarvis, biography: "I. am. SNARVIS!")
  @bob = User.create(email: "bob@bobland.net", password: "123456", password_confirmation: "123456")
  @alice = User.create(email: "alice@alice.net", password: "123456", password_confirmation: "123456")
  @bob.confirm
  @alice.confirm
  @bob.create_profile
  @alice.create_profile
end

Given(/^they have gone to the profile for Jorvis$/) do
  visit profile_path(@jorvis)
end

When(/^They fill in the "(.*?)" field with "(.*?)"$/) do |field, content|
  fill_in field, with: content
end

Then(/^the page contains "(.*?)"$/) do |content|
  expect(page).to have_content(content)
end