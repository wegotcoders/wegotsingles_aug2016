Given(/^sample conversations have been created$/) do
  @convo = @alice.conversations.create(recipient: @bob)
end

Given(/^sample messages have been created$/) do
  @convo.messages.create(user: @alice, body: "Hey Bobby, do you like dags?")
  @convo.messages.create(user: @bob, body: "Sure, Alice, lav a dag")
  @message_count = @convo.messages.count
end

Given(/^a conversation already exists between "(.*?)" and "(.*?)"$/) do |user_1, user_2|
  expect(@alice.my_conversations.last.recipient).to eq @bob
  expect(@bob.my_conversations.last.sender).to eq @alice
  expect(@bob.my_conversations.count).to eq 1
  expect(@alice.my_conversations.count).to eq 1
end

Given(/^they have logged in as "(.*?)"$/) do |email|
  @user = User.find_by(email: email)
  page.set_rack_session('warden.user.user.key' => User.serialize_into_session(@user))
  @profile = @user.profile
end

When(/^they navigate to the "(.*?)" page$/) do |page|
  visit get_named_route(page)
end

When(/^they navigate to the specific "(.*?)" page$/) do |page|
  case page
  when 'conversation'
    visit get_named_route(page, @convo)
  when 'profile'
    visit get_named_route(page, @alice.profile)
  end
end

When(/^they click "(.*?)"$/) do |this|
  click_on this
end

When(/^they press "([^"]*)"(?: on "([^"]*)"|)$/) do |key, css_selector|
  css_selector ||= 'body'
  case key
  when "ENTER"
    keycode = 13
  when "TAB"
    keycode = 9
  when "SPACE"
    keycode = 32
  end
  keypress_script = "var e = $.Event('keydown', { keyCode: #{keycode} }); $('#{css_selector}').trigger(e);"
  page.driver.execute_script(keypress_script)
end

Then(/^a new Conversaton is created$/) do
  sleep 1
  expect(@bob.conversations.count).to eq 1
  expect(@alice.my_conversations.count).to eq 1
end

Then(/^a new Conversation is not created$/) do
  sleep 1
  expect(@bob.my_conversations.count).to eq 1
  expect(@alice.my_conversations.count).to eq 1
end

Then(/^a new Message is created$/) do
  expect(@bob.my_conversations.last.messages.last.body).to eq("Alright Chicken!")
  expect(@alice.my_conversations.last.messages.last.body).to eq("Alright Chicken!")
end

Then(/^they are redirected to the specific "(.*?)" page$/) do |page|
  sleep 1
  expect(current_path).to eq(get_named_route(page, @bob.my_conversations.last))
end




