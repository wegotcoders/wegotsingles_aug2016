Given(/^Sample users have been created$/) do
  @snarvis = User.new(email: "snarvis@barvis.com", password: "123456",
                      email: "SexySnarvis", date_of_birth: "1990/01/01")
  @snarvis.skip_confirmation!
  @snarvis.confirm
  @snarvis.save!
  Profile.create(user: @snarvis, biography: "I. am. SNARVIS!")
end

Given(/^They have gone to the route "(.*?)"$/) do |page|
 visit get_named_route(page)
end

Given(/^They are not signed in$/) do

end

When(/^They set the "(.*?)" date to "(.*?)"$/) do |date, value|
  page.find(date).set(value)
end

When(/^They click the "(.*?)" button$/) do |button|
  click_on button
end

Then(/^A new user is created$/) do
  expect(User.find_by(email: "SexyBarvis69")).to be_truthy
end

Then(/^They are redirected to their$/) do
  pending # express the regexp above with the code you wish you had
end

Given(/^They are logged in as "(.*?)"$/) do |email|
  @user = User.find_by(email: email)
  page.set_rack_session('warden.user.user.key' => User.serialize_into_session(@user))
end

Then(/^Their profile is updated$/) do
  pending # express the regexp above with the code you wish you had
  # %%%
  expect(@snarvis.date_of_birth).to eq("Mon, 01 Jan 1998")
end

When(/^Sleep (\d+) seconds$/) do |time|
  sleep(time.to_i)
end



