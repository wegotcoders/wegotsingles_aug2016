
When(/^they fill in the "(.*?)" field with "(.*?)"$/) do |label, content|
  fill_in label, with: content
end

Then(/^a new "(.*?)" is created$/) do |object_name|
  sleep 1
  contant = ObjectSpace.const_get(object_name)
  expect(contant.count).to eq(3)
end

Given(/^their latest message is unread$/) do
  expect(@bob.my_conversations.last.messages.last.is_read).to be_falsey
end

Then(/^the page contains the element "(.*?)"$/) do |css_selector|
  page.should have_css(css_selector)
end


When(/^they click on the "(.*?)" link$/) do |this|
  click_link this
end

Then(/^the message is updated as read$/) do
  expect(@bob.my_conversations.last.messages.last.is_read).to be_truthy
end

