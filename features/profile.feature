Feature: Profile
  As a user, I should be able to view my own profile
  As a user, I should be able to view another user's profile
  As a user, I should be able to edit my profile biography
  As a user, I should be able to enter my height and weight in metric or imperial measurements

  Scenario: Jorvis views his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    Then the page contains "Signed in as SexyJorvis"

  # Scenario: Jorvis views another user's profile
    
  Scenario: Jorvis edits the biography on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Bio" button
      And they fill in the "profile[biography]" field with "I'm a real good guy and I definitely don't drown puppies"
      And they click the "Save bio" button
    Then Jorvis' biography is updated

  Scenario: Jorvis indicates his level of education on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Background" button
      And they choose "Hindu" from the "profile[religion]" dropdown list
      And they click the "Save info 2" button
    # Then Jorvis' "religion" is set to "Hindu"
    Then Jorvis' religion is set to Hindu

  Scenario: Jorvis edits the languages on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Background" button
      And they click the "english" checkbox for "profile_languages_"
      And they click the "french" checkbox for "profile_languages_"
      And they click the "spanish" checkbox for "profile_languages_"
      And they click the "japanese" checkbox for "profile_languages_"
      And they click the "elvish" checkbox for "profile_languages_"
      And they click the "minion" checkbox for "profile_languages_"
      And they click the "Save info" button
    # Then Jorvis' "languages" is set to "["English", "French", "Spanish", "Japanese", "Elvish", "Minion"]"
    Then Jorvis' languages is set to English, French, Spanish, Japanese, Elvish, Minion

  Scenario: Jorvis edits the weight on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Basics" button
      And they fill in the "profile[weight]" field with "95"
      And they click the "Save info" button
    # Then Jorvis' "weight" is set to "180"
    Then Jorvis' weight is set to 95kg

  Scenario: Jorvis edits the height on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Basics" button
      And they fill in the "profile[height]" field with "180"
      And they click the "Save info" button
    # Then Jorvis' "height" is set to "180"
    Then Jorvis' height is set to 180cm

  Scenario: Jorvis specifies what he is looking for on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Basics" button
      And they click the "male" checkbox for "profile_orientation_"
      And they click the "female" checkbox for "profile_orientation_"
      And they click the "Save info" button
    # Then Jorvis' "orientation" is set to "["Male", "Female"]"
    Then Jorvis' interested in is set to Male and Female

  Scenario: Jorvis indicates whether he smokes on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Details" button
      And they choose "Frequently" from the "profile[smokes]" dropdown list
      And they click the "Save info 3" button
    # Then Jorvis' "smokes" is set to "Frequently"
    Then Jorvis' smokes is set to Frequently

  Scenario: Jorvis indicates whether he drinks on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Details" button
      And they choose "Sometimes" from the "profile[drinks]" dropdown list
      And they click the "Save info 3" button
    # Then Jorvis' "drinks" is set to "Sometimes"
    Then Jorvis' drinks is set to Sometimes

  Scenario: Jorvis indicates his level of education on his profile
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they click the "Edit Details" button
      And they choose "High School" from the "profile[education]" dropdown list
      And they click the "Save info 3" button
    # Then Jorvis' "education" is set to "High School"
    Then Jorvis' education is set to High School

  Scenario: Jorvis uploads a picture
    Given sample users have been created
      And they are logged in as "jorvis@barvis.com"
      And they have gone to the profile for Jorvis
    When they fill in the upload picture form with a valid image
    Then they click the "Upload" button
      And they have gone to the profile for Jorvis
      And the page contains the id of \"my_profile_pic\" with content "my profile pic"

