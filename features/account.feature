Feature: Account
  As a user, I should be able to provide a username when I register
  As a user, I should be able to provide a D.O.B when I register





  Scenario: Barvis registers
    Given They have gone to the route "new user registration"
      # %%%
      # And They are not signed in
    When They fill in the "user_email" field with "barvis@gmail.com"
      And They fill in the "user_password" field with "123456"
      And They fill in the "user_password_confirmation" field with "123456"
      And They fill in the "user_username" field with "SexyBarvis69"
      And They set the "#user_date_of_birth_1i" date to "1955/01/01"
      And They click the "Sign up" button
    Then A new user is created
      # %%% And They are redirected to their dashboard

  # Scenario: Barvis clicks on the confirmation link in his registration email
  #   Given 

  Scenario: Snarvis edits his date of birth
    Given Sample users have been created
      And They are logged in as "snarvis@barvis.com"
      And They have gone to the route "edit user registration"
    When They fill in the "user_current_password" field with "123456"   
      And They set the "#user_date_of_birth_1i" date to "1998/01/01"
      And They click the "Update" button
      And Sleep 2 seconds
    Then Their profile is updated
  